﻿using UnityEngine;
using System.Collections;

public class PlayerMovementScript : MonoBehaviour 
{
    #region Variables
    // SerializeField exposes this value to the Editor, but not to other Scripts!
    // It is "pseudo public"
    // HorizontalPlayerAcceleration indicates how fast we accelerate Horizontally
    [SerializeField] private float _horizontalPlayerAcceleration = 5000f; //HorizontalPlayerAcceleration indicates how fast we accelerate Horizontally
    private Rigidbody2D _rigidbody;
    private Vector2 _forceToAdd;
    InputHandler InputHandler;
    #endregion
    #region Unity Functions
    void Start() 
    {
        //Find Components
        _rigidbody = GetComponent<Rigidbody2D>();
        InputHandler = GetComponent<InputHandler>();
    }
    private void FixedUpdate()
    {
        Move();
    }
    #endregion
    #region Move
    public void Move()
    {
        if (InputHandler._movementInputPressed)
        {
            _forceToAdd = Vector2.right * InputHandler._movementDirection.x * _horizontalPlayerAcceleration * Time.deltaTime;
            _rigidbody.AddForce(_forceToAdd);
        }
    }
    #endregion
}

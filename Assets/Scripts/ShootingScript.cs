﻿using UnityEngine;
using System.Collections;

public class ShootingScript : MonoBehaviour
{

    [SerializeField]
    private GameObject m_bullet;
    private float _lastFiredTime = 0f;
    [SerializeField]
    private float _fireDelay = 1f;
    private float _bulletOffset = 2f;
    private InputHandler InputHandler;
    void Start()
    {
        InputHandler = GetComponent<InputHandler>();

        // Do some math to perfectly spawn bullets in front of us
        _bulletOffset = GetComponent<Renderer>().bounds.size.y / 2 // Half of our size
            + m_bullet.GetComponent<Renderer>().bounds.size.y / 2; // Plus half of the bullet size
    }

    // Update is called once per frame
    void Update()
    {
        if (InputHandler._clickInput)
        {
            Shoot();
        }
    }
    public void Shoot()
    {
        float m_currentTime = Time.time;

        // Have a delay so we don't shoot too many bullets
        if (m_currentTime - _lastFiredTime > _fireDelay)
        {
            Vector2 m_spawnPosition = new Vector2(transform.position.x, transform.position.y + _bulletOffset);

            Instantiate(m_bullet, m_spawnPosition, transform.rotation);

            _lastFiredTime = m_currentTime;
        }
    }
    /// <summary>
    /// SampleMethod is a sample of how to use abstraction by
    /// specification. It converts a provided integer to a float.
    /// </summary>
    /// <param name="number">any integer</param>
    /// <returns>the number parameter as a float</returns>
    public float SampleMethod(int number) {
        return number;
    }

}
